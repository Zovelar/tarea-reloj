var reloj1 = Reloj()
var reloj2 = Reloj()

reloj1.setReloj(horas: 5000)
reloj2.setReloj(horas: 5000)
print(reloj1.toString())
print(reloj2.toString())
reloj1.addReloj(reloj: reloj2)
print(reloj1.toString())

class Reloj{
    var hora:Int = 0
    var minutos:Int = 0
    var segundos:Int = 0

    //setters
    func setReloj( horas: Int )
    {
        hora=horas/3600
        minutos=(horas%3600)/60
        segundos=horas-hora*3600-minutos*60
    }
    func setHoras( horaUsuario: Int )
    {
        hora=horaUsuario
    }
    func setMinutos( minutosUsuario: Int )
    {
        minutos=minutosUsuario
    }
    func setSegundos( segundosUsuario: Int )
    {
        segundos=segundosUsuario
    }
    //getters
    func getHoras() -> Int
    {
        return hora
    }
    func getMinutos() -> Int
    {
        return minutos
    }
    func getSegundos() -> Int
    {
        return segundos
    }
    func tick()
    {
        if segundos+1 < 60
        {
            segundos=segundos+1
        }
        else
        {
            segundos=0
        }
    }
    func addReloj(reloj: Reloj)
    {
        let horaRelojActual=hora*3600 + minutos*60 + segundos
        let horaRelojNuevo=reloj.getHoras()*3600 + reloj.getMinutos()*60 + reloj.getSegundos()
        let relojSuma = Reloj()
        relojSuma.setReloj(horas: horaRelojActual+horaRelojNuevo)
        hora=relojSuma.getHoras()
        minutos=relojSuma.getMinutos()
        segundos=relojSuma.getSegundos()
    }
    func toString() -> String
    {
        return "[\(hora):\(minutos):\(segundos)]"
    }
    func tickDecremento()
    {
        if segundos-1 >= 0
        {
            segundos=segundos-1
        }
    }
}
/*
var reloj1 = Reloj()
var reloj2 = Reloj()

reloj1.setReloj(horas: 5000)
reloj2.setReloj(horas: 5000)
print(reloj1.toString())
print(reloj2.toString())
reloj1.addReloj(reloj: reloj2)
print(reloj1.toString())
*/